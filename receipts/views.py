from django.shortcuts import render, get_list_or_404
from receipts.models import Receipt


# Create your views here.
def receipts_list(request):
    lists = Receipt.objects.all()
    context = {"receipts_list": lists}
    return render(request, "receipts/list.html", context)
